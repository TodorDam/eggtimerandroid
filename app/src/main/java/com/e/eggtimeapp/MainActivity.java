package com.e.eggtimeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int NECESSARY_DELAY = 100;
    private final int ONE_SECOND = 1000;
    private final int ONE_MINUTE = 60;
    private final int NUMBER_OF_MINUTES = 4;
    private final int TIMER_MAX_TIME_IN_SECONDS = NUMBER_OF_MINUTES * ONE_MINUTE;
    private final int DEFAULT_START_VALUE = 1;

    private SeekBar seekBar;
    private TextView timerView;
    private boolean isTimerStarted = false;
    private CountDownTimer timer = null;
    private int selectedTime;
    private ImageView notesImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = findViewById(R.id.seekBarId);
        seekBar.setMax(TIMER_MAX_TIME_IN_SECONDS);
        seekBar.setProgress(DEFAULT_START_VALUE * ONE_MINUTE);

        timerView = findViewById(R.id.timeLeftId);

        updateTimer(DEFAULT_START_VALUE * ONE_MINUTE, timerView);

        notesImg = findViewById(R.id.notesImgId);
        notesImg.setRotation(15f);
        notesImg.setVisibility(View.INVISIBLE);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateTimer(progress, timerView);
                selectedTime = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }


    public void startTimer(View view) {

        final Button buttonStart = (Button) findViewById(R.id.startTimerId);

        if (!isTimerStarted) {

            seekBar.setEnabled(false);
            buttonStart.setText(getString(R.string.stopTimer));
            timer = new CountDownTimer(seekBar.getProgress() * ONE_SECOND + NECESSARY_DELAY, ONE_SECOND) {

                @Override
                public void onTick(long millisUntilFinished) {
                    updateTimer((int) (millisUntilFinished / ONE_SECOND), timerView);
                }

                @Override
                public void onFinish() {
                    seekBar.setEnabled(true);
                    updateTimer(0, timerView);
                    buttonStart.setText(getString(R.string.startTimer));
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.cartoonbirds2);
                    mp.start();
                    notesImg = findViewById(R.id.notesImgId);
                    Animation an = new AlphaAnimation(0.7f, 0);
                    an.setDuration(500);
                    an.setRepeatCount(11);
                    notesImg.startAnimation(an);
                }
            }.start();
            isTimerStarted = true;
        } else {

            timer.cancel();
            seekBar.setProgress(selectedTime);
            updateTimer(selectedTime, timerView);
            buttonStart.setText(getString(R.string.startTimer));
            seekBar.setEnabled(true);
            isTimerStarted = false;
        }
    }

    private int calcSeconds(int progress, int minutes) {
        return progress - minutes * ONE_MINUTE;
    }

    private int calcMinutes(int progress) {
        return progress / ONE_MINUTE;
    }

    private String formatSeconds(int seconds) {
        return seconds < 10 ? "0" + seconds : String.valueOf(seconds);
    }

    private String formatMinutes(int minutes) {
        return minutes < 10 ? "0" + minutes : String.valueOf(minutes);
    }

    private void updateTimer(int progress, TextView timerView) {
        int minutes, seconds;
        minutes = calcMinutes(progress);
        seconds = calcSeconds(progress, minutes);

        String minutesPrint = formatMinutes(minutes);
        String secondsPrint = formatSeconds(seconds);

        timerView.setText(String.format("%s : %s", minutesPrint, secondsPrint));
    }
}
